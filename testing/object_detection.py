import math
import numpy as np
import six.moves.urllib as urllib
import tarfile
import tensorflow as tf
import zipfile
import cv2
import matplotlib
import time

from distutils.version import StrictVersion
from collections import defaultdict
from io import StringIO
from matplotlib import pyplot as plt
from PIL import Image

# This is needed since the notebook is stored in the object_detection folder.
import sys
import os
from os.path import dirname, join
pth = join(dirname(os.getcwd()),'training','models','research')
sys.path.insert(0, pth)
sys.path.insert(1, join(pth,'slim'))
sys.path.append("..")

from object_detection.utils import ops as utils_ops

#if StrictVersion(tf.__version__) < StrictVersion('1.9.0'):
#  raise ImportError('Please upgrade your TensorFlow installation to v1.9.* or later!')

from object_detection.utils import label_map_util
from object_detection.utils import visualization_utils as vis_util


def sortPoints(points):

    sorted_points = [[0,0]]

    a = points[0]
    min_xy = a[0]+a[1]
    iaux=0
    for i in range(0, 4):
        if min_xy > points[i][0]+ points[i][1]:  # min
            min_xy = points[i][0]+ points[i][1]
            a = points[i]
            iaux=i 
    sorted_points.append(a)
    sorted_points.pop(0)
    points.pop(iaux)

    b = points[0]
    minx= b
    miny= b
    ix=0
    iy=0
    for i in range(0, 3):
        if minx[0] > points[i][0]:  # minX
            minx = points[i]
            ix= i
        if miny[1] > points[i][1]:  # minY
            miny = points[i]
            iy= i
    if minx[1] <= miny[0]:
        b = minx
        iaux = ix
    else:
        b = miny
        iaux = iy
    sorted_points.append(b)
    points.pop(iaux)

    c = points[0]
    maxx= c
    maxy= c
    ix=0
    iy=0
    for i in range(0, 2):
        if maxx[0] < points[i][0]:  # maxX
            maxx = points[i]
            ix= i
        if maxy[1] < points[i][1]:  # maxY
            maxy = points[i]
            iy= i
    if maxx[1] >= maxy[0]:
        c = maxx
        iaux = ix
    else:
        c = maxy
        iaux = iy
    sorted_points.append(c)
    points.pop(iaux)

    d = points[0]
    sorted_points.append(d)
    points.pop(0)
 
    return sorted_points

from PyQt5.QtWidgets import QApplication
app = QApplication(sys.argv)
screen = app.screens()[0]
dpi = screen.physicalDotsPerInch()
app.quit()

MY_DPI = dpi

MODEL_NAME = 'diana_inference_graph'
PATH_TO_FROZEN_GRAPH = './../training/'+MODEL_NAME + '/frozen_inference_graph.pb'
PATH_TO_LABELS = './../data/diana-detection.pbtxt'


detection_graph = tf.compat.v1.Graph()
with detection_graph.as_default():
  od_graph_def = tf.compat.v1.GraphDef()
  with tf.compat.v1.gfile.GFile(PATH_TO_FROZEN_GRAPH, 'rb') as fid:
    serialized_graph = fid.read()
    od_graph_def.ParseFromString(serialized_graph)
    tf.import_graph_def(od_graph_def, name='')


category_index = label_map_util.create_category_index_from_labelmap(PATH_TO_LABELS, use_display_name=True)

def load_image_into_numpy_array(image):
    (im_width, im_height) = image.size
    return np.array(image.getdata()).reshape((im_height, im_width, 3)).astype(np.uint8)

# Establece path de imagenes a detectar
PATH_TO_TEST_IMAGES_DIR = './../img/warped/test'

# Establece el tamano
IMAGE_SIZE = (576, 512)

# Paso de parametros; si se especifican, se predicen las imagenes de la carpeta test de N_INI a N_FIN
if(len(sys.argv)>=3):
    ini = int(sys.argv[1])
    fin = int(sys.argv[2])
    TEST_IMAGE_PATHS = [ os.path.join(PATH_TO_TEST_IMAGES_DIR, '{}.jpeg'.format(i)) for i in range(ini,fin+1)]
else:
    TEST_IMAGE_PATHS = [ os.path.join(PATH_TO_TEST_IMAGES_DIR, '{}.jpeg'.format(i)) for i in range(1,4)]



c = 1
with detection_graph.as_default():
    with tf.compat.v1.Session(graph=detection_graph) as sess:
        for image_path in TEST_IMAGE_PATHS:
            image = Image.open(image_path)
            image_np = load_image_into_numpy_array(image)
            image_np_expanded = np.expand_dims(image_np, axis=0)
            image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
            boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
            scores = detection_graph.get_tensor_by_name('detection_scores:0')
            classes = detection_graph.get_tensor_by_name('detection_classes:0')
            num_detections = detection_graph.get_tensor_by_name('num_detections:0')

            # Calculo del tiempo de ejecucion de prediccion de la imagen
            start_time = time.time()

            (boxes, scores, classes, num_detections) = sess.run(
                [boxes, scores, classes, num_detections],
                feed_dict = {image_tensor: image_np_expanded})
            
            print("\n\n--- %s segundos ---" % (time.time() - start_time))
            
            # Llamada a la funcion modificada de return_coordinates() para obtener las coordenadas
            # de los objectos detectados (esquinas y agujeros)
            coordinates = vis_util.return_coordinates(
                        image_np,
                        np.squeeze(boxes),
                        np.squeeze(classes).astype(np.int32),
                        np.squeeze(scores),
                        category_index,
                        use_normalized_coordinates=True,
                        line_thickness=8,
                        min_score_thresh=0.80)

            esquinas = []
            agujeros = []

            # Ajuste de coordenadas al centro y Clasificacion de los objectos en agujeros y esquinas
            for ind, coord in enumerate(coordinates, start=0):
                if(coordinates[ind][0] == 'esquina'):
                    esquinas.append(coord[1:])
                else:
                    agujeros.append(coord[1:])
                    

            # Centra los puntos de las esquinas y agujeros a partir de los puntos de origen
            # y reduce dimension de las esquinas a (X,Y)
            esquinasCentro = []
            for ind, coords in enumerate(esquinas, start=0):
                esquinasCentro.append([(coords[0]+coords[1])/2, (coords[2]+coords[3])/2])

            agujerosCentro = []
            for ind, coords in enumerate(agujeros, start=0):
                agujerosCentro.append([(coords[0]+coords[1])/2, (coords[2]+coords[3])/2])
            
            if len(esquinasCentro)< 4 :
                print('no se han detectado suficientes esquinas')
            else :

                # Ordena las esquinas
                esquinasCentro = sortPoints(esquinasCentro)
                tL = esquinasCentro[0]
                dL = esquinasCentro[1]
                dR = esquinasCentro[2]
                tR = esquinasCentro[3]
                offset = tL
                width=tR[0]-tL[0]
                height=dL[1]-tL[1]
                print("ESQUINAS= "+str(esquinasCentro))
                print("AGUJEROS= "+str(agujerosCentro)+"\n")

                # Calcula el centro aproximado de la diana
                centro = [(tL[0]+tR[0])/2,(tL[1]+dL[1])/2]
                print("CENTRO= "+str(centro))

                #AGUJEROSCENTRO & CENTRO/TL & W & H
                punt=[]

                # Calcula la puntuacion de cada agujero en base a su posicion respecto al centro
                # segun el area del anillo en el que se encuentra. Este area esta descrita por 
                # un ovalo con el centro en el centro de las esquinas, con radio proporcional
                # al ancho y alto de la diana para calcular correctamente las puntuaciones en
                # el caso de que la diana se encuentre deformada.
                for index, punto in enumerate(agujerosCentro, start=0):
                    if ( ( (punto[0]-centro[0])**2 / ((0.5-0.41)*width)**2 ) + ( (punto[1]-centro[1])**2 / ((0.5-0.38)*height)**2) <= 1 ):
                        punt.append(3)
                    elif ( ( (punto[0]-centro[0])**2 / ((0.5-0.29)*width)**2 ) + ( (punto[1]-centro[1])**2 / ((0.5-0.21)*height)**2) <= 1 ):
                        punt.append(2)
                    elif ( ( (punto[0]-centro[0])**2 / ((0.5-0.17)*width)**2 ) + ( (punto[1]-centro[1])**2 / ((0.5-0.08)*height)**2) <= 1 ):
                        punt.append(1)
                    else:
                        punt.append(0)
                print("PUNTUACION= "+str(sum(punt)))
                print(punt)

                vis_util.visualize_boxes_and_labels_on_image_array(
                image_np,
                np.squeeze(boxes),
                np.squeeze(classes).astype(np.int32),
                np.squeeze(scores),
                agujerosCentro,
                punt,
                category_index,
                use_normalized_coordinates=True,
                line_thickness=4)

                matplotlib.use('Qt5Agg',warn=False,force=True)
                #matplotlib.use('Agg')
            
                # Creamos la figura con el tamano fijo
                plt.figure(figsize=(IMAGE_SIZE[0]/MY_DPI,IMAGE_SIZE[1]/MY_DPI))

                # Mostramos la puntuacion
                txt = 'PUNTUACION: '+str(sum(punt))
                plt.text(20, 20, txt, fontsize=20, bbox=dict(facecolor='red', alpha=0.5))
                #for ind, coord in enumerate(agujerosCentro, start=0):
                #    txt = 'agujero'+str(ind)+': ('+str(coord[0])+', '+str(coord[1])+') '+' PUNT='+str(punt[ind])+' ['+str('{0:.2f}'.format(coordinates[ind][5]))+'%]'
                #    plt.text(20, 20 + ind*40, txt, fontsize=10, bbox=dict(facecolor='aqua', alpha=0.5))
                punt.clear()
                plt.imshow(image_np)
                if (len(sys.argv)>=4):
                    if (sys.argv[3]=='--mode=save'):
                        plt.savefig(os.getcwd()+'/predicts/predict'+str(c)+'.jpeg', dpi=MY_DPI*2)
                    elif (sys.argv[3]=='--mode=show'):
                        plt.show()
                else :
                    plt.show()
                c = c + 1