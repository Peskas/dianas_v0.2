import cv2
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image  
import sys


def sortPoints(points):
    print(points)

    sorted_points = [[0,0]]
    print(sorted_points)

    a = points[0]
    min_xy = a[0]+a[1]
    iaux=0
    for i in range(0, 4):
        if min_xy > points[i][0]+ points[i][1]:  # min
            min_xy = points[i][0]+ points[i][1]
            a = points[i]
            iaux=i 

    print(a)

    sorted_points.append(a)
    sorted_points.pop(0)
    points.pop(iaux)

    print(sorted_points)
    print(points)
 

    print('-----------')


    b = points[0]
    minx= b
    miny= b
    ix=0
    iy=0
    for i in range(0, 3):
        if minx[0] > points[i][0]:  # minX
            minx = points[i]
            ix= i
        if miny[1] > points[i][1]:  # minY
            miny = points[i]
            iy= i
    if minx[1] <= miny[0]:
        b = minx
        iaux = ix
    else:
        b = miny
        iaux = iy
    print(b)

    sorted_points.append(b)
    points.pop(iaux)

    print(sorted_points)
    print(points)




    print('-----------')

    c = points[0]
    maxx= c
    maxy= c
    ix=0
    iy=0
    for i in range(0, 2):
        if maxx[0] < points[i][0]:  # minX
            maxx = points[i]
            ix= i
        if maxy[1] < points[i][1]:  # minY
            maxy = points[i]
            iy= i
    print(maxx[1])
    print(maxy[0])
    if maxx[1] >= maxy[0]:
        c = maxx
        iaux = ix
    else:
        c = maxy
        iaux = iy
    print(c)

    sorted_points.append(c)
    points.pop(iaux)

    print(sorted_points)
    print(points)


    d = points[0]
    sorted_points.append(d)
    points.pop(0)

    print(sorted_points)
    print(points)
 
    return sorted_points


def get_destination_points(corners):
    """
    -Get destination points from corners of warped images
    -Approximating height and width of the rectangle: we take maximum of the 2 widths and 2 heights

    Args:
        corners: list

    Returns:
        destination_corners: list
        height: int
        width: int

    """

    w1 = np.sqrt((corners[0][0] - corners[1][0]) ** 2 +
                 (corners[0][1] - corners[1][1]) ** 2)
    w2 = np.sqrt((corners[2][0] - corners[3][0]) ** 2 +
                 (corners[2][1] - corners[3][1]) ** 2)
    w = max(int(w1), int(w2))

    h1 = np.sqrt((corners[0][0] - corners[2][0]) ** 2 +
                 (corners[0][1] - corners[2][1]) ** 2)
    h2 = np.sqrt((corners[1][0] - corners[3][0]) ** 2 +
                 (corners[1][1] - corners[3][1]) ** 2)
    h = max(int(h1), int(h2))

    destination_corners = np.float32(
        [(0, h - 1), (0, 0),  (w - 1, 0), (w - 1, h - 1)])

    print('\nThe destination points are: \n')
    for index, c in enumerate(destination_corners):
        character = chr(65 + index) + "'"
        print(character, ':', c)

    print('\nThe approximated height and width of the original image is: \n', (h, w))
    return destination_corners, h, w


def unwarp(img, src, dst):
    """

    Args:
        img: np.array
        src: list
        dst: list

    Returns:
        un_warped: np.array

    """
    h, w = img.shape[:2]
    H, _ = cv2.findHomography(
        src, dst, method=cv2.RANSAC, ransacReprojThreshold=3.0)
    print('\nThe homography matrix is: \n', H)
    un_warped = cv2.warpPerspective(img, H, (w, h), flags=cv2.INTER_LINEAR)

    # plot

    f, (ax1, ax2) = plt.subplots(1, 2, figsize=(15, 8))
    # f.subplots_adjust(hspace=.2, wspace=.05)
    ax1.imshow(img)
    ax1.set_title('Original Image')

    x = [src[0][0], src[1][0], src[2][0], src[3][0], src[0][0]]
    y = [src[0][1], src[1][1], src[2][1], src[3][1], src[0][1]]

    ax2.imshow(img)
    ax2.plot(x, y, color='yellow', linewidth=3)
    ax2.set_ylim([h, 0])
    ax2.set_xlim([0, w])
    ax2.set_title('Target Area')

    plt.show()
    return un_warped


def apply_filter(image):
    """
    Define a 5X5 kernel and apply the filter to gray scale image
    Args:
        image: np.array

    Returns:
        filtered: np.array

    """
    gray = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
    kernel = np.ones((2, 2), np.float32) / 15
    filtered = cv2.filter2D(gray, -1, kernel)
    plt.imshow(cv2.cvtColor(filtered, cv2.COLOR_BGR2RGB))
    plt.title('Filtered Image')
    plt.show()
    return filtered


def apply_threshold(filtered):
    """
    Apply OTSU threshold

    Args:
        filtered: np.array

    Returns:
        thresh: np.array

    """
    ret, thresh = cv2.threshold(filtered, 127,255, cv2.THRESH_OTSU)

 

    plt.imshow(cv2.cvtColor(thresh, cv2.COLOR_BGR2RGB))
    plt.title('After applying OTSU threshold')
    plt.show()
    return thresh


def detect_contour(img, image_shape):
    """

    Args:
        img: np.array()
        image_shape: tuple

    Returns:
        canvas: np.array()
        cnt: list

    """
    canvas = np.zeros(image_shape, np.uint8)
    contours, hierarchy = cv2.findContours(
        img, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
    cnt = sorted(contours, key=cv2.contourArea, reverse=True)[0]
    cv2.drawContours(canvas, cnt, -1, (0, 255, 255), 3)
    plt.title('Largest Contour')
    plt.imshow(canvas)
    plt.show()

    return canvas, cnt


def detect_corners_from_contour(canvas, cnt):
    """
    Detecting corner points form contours using cv2.approxPolyDP()
    Args:
        canvas: np.array()
        cnt: list

    Returns:
        approx_corners: list

    """
    epsilon = 0.02 * cv2.arcLength(cnt, True)
    approx_corners = cv2.approxPolyDP(cnt, epsilon, True)
    cv2.drawContours(canvas, approx_corners, -1, (255, 255, 0), 10)
    approx_corners = sorted(np.concatenate(approx_corners).tolist())
    approx_corners = [approx_corners[i] for i in [0, 1, 2, 3]]
    print('\nThe corner points are ...\n')
    for index, c in enumerate(approx_corners):
        character = chr(65 + index)
        print(character, ':', c)
        cv2.putText(canvas, character, tuple(
            c), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2, cv2.LINE_AA)

    approx_corners = sortPoints(approx_corners)

    # Rearranging the order of the corner points

    plt.imshow(canvas)
    plt.title('Corner Points: Douglas-Peucker')
    plt.show()
    return approx_corners


def example():
    """
    Skew correction using homography and corner detection using contour points
    Returns: None

    """

    name = str(sys.argv[1])
    image = cv2.imread('images/'+name)
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    plt.imshow(image)
    plt.title('Original Image')
    plt.show()

    filtered_image = apply_filter(image)
    threshold_image = apply_threshold(filtered_image)

    cnv, largest_contour = detect_contour(threshold_image, image.shape)
    corners = detect_corners_from_contour(cnv, largest_contour)

    destination_points, h, w = get_destination_points(corners)
    un_warped = unwarp(image, np.float32(corners), destination_points)

    cropped = un_warped[0:h, 0:w]
    f, (ax1, ax2) = plt.subplots(1, 2, figsize=(15, 8))
    # f.subplots_adjust(hspace=.2, wspace=.05)
    ax1.imshow(un_warped)

    #ax2.axes.get_xaxis().set_visible(False)
    #ax2.axes.get_yaxis().set_visible(False)
    cropped = cv2.resize(cropped, dsize=(500,500) , interpolation= cv2.INTER_CUBIC )  
    ax2.imshow(cropped)
 
  

    plt.show()

    


if __name__ == '__main__':

    example()
