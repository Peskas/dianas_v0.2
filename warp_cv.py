import cv2
import numpy as np
import matplotlib.pyplot as plt
import scipy.misc
import multiprocessing as mp
from os import walk
import os
from PIL import Image
import sys
from PyQt5.QtWidgets import QApplication



def ls(ruta):
    dir, subdirs, archivos = next(walk(ruta))
    return archivos


def detect_contour(img, image_shape):
    """

    Args:
        img: np.array()
        image_shape: tuple

    Returns:
        canvas: np.array()
        cnt: list

    """
    canvas = np.zeros(image_shape, np.uint8)
    contours, hierarchy = cv2.findContours(
        img, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
    cnt = sorted(contours, key=cv2.contourArea, reverse=True)[0]
    cv2.drawContours(canvas, cnt, -1, (0, 255, 255), 3)

    return canvas, cnt


def detect_corners_from_contour(canvas, cnt):
    """
    Detecting corner points form contours using cv2.approxPolyDP()
    Args:
        canvas: np.array()
        cnt: list

    Returns:
        approx_corners: list

    """
    epsilon = 0.02 * cv2.arcLength(cnt, True)
    approx_corners = cv2.approxPolyDP(cnt, epsilon, True)
    cv2.drawContours(canvas, approx_corners, -1, (255, 255, 0), 10)
    approx_corners = sorted(np.concatenate(approx_corners).tolist())
    # Rearranging the order of the corner points
    approx_corners = [approx_corners[i] for i in [0, 1, 2, 3]]
    approx_corners = sortPoints(approx_corners)

    return approx_corners

# Funcion que, dados 4 puntos, los ordena de forma que formen un rectangulo entre ellos
def sortPoints(points):

    sorted_points = [[0,0]]

    a = points[0]
    min_xy = a[0]+a[1]
    iaux=0
    for i in range(0, 4):
        if min_xy > points[i][0]+ points[i][1]:  # min
            min_xy = points[i][0]+ points[i][1]
            a = points[i]
            iaux=i 
    sorted_points.append(a)
    sorted_points.pop(0)
    points.pop(iaux)

    b = points[0]
    minx= b
    miny= b
    ix=0
    iy=0
    for i in range(0, 3):
        if minx[0] > points[i][0]:  # minX
            minx = points[i]
            ix= i
        if miny[1] > points[i][1]:  # minY
            miny = points[i]
            iy= i
    if minx[1] <= miny[0]:
        b = minx
        iaux = ix
    else:
        b = miny
        iaux = iy
    sorted_points.append(b)
    points.pop(iaux)

    c = points[0]
    maxx= c
    maxy= c
    ix=0
    iy=0
    for i in range(0, 2):
        if maxx[0] < points[i][0]:  # maxX
            maxx = points[i]
            ix= i
        if maxy[1] < points[i][1]:  # maxY
            maxy = points[i]
            iy= i
    if maxx[1] >= maxy[0]:
        c = maxx
        iaux = ix
    else:
        c = maxy
        iaux = iy
    sorted_points.append(c)
    points.pop(iaux)

    d = points[0]
    sorted_points.append(d)
    points.pop(0)
 
    return sorted_points


def get_destination_points(corners):
    """
    -Get destination points from corners of warped images
    -Approximating height and width of the rectangle: we take maximum of the 2 widths and 2 heights

    Args:
        corners: list

    Returns:
        destination_corners: list
        height: int
        width: int

    """

    w1 = np.sqrt((corners[0][0] - corners[1][0]) ** 2 +
                 (corners[0][1] - corners[1][1]) ** 2)
    w2 = np.sqrt((corners[2][0] - corners[3][0]) ** 2 +
                 (corners[2][1] - corners[3][1]) ** 2)
    w = max(int(w1), int(w2))

    h1 = np.sqrt((corners[0][0] - corners[2][0]) ** 2 +
                 (corners[0][1] - corners[2][1]) ** 2)
    h2 = np.sqrt((corners[1][0] - corners[3][0]) ** 2 +
                 (corners[1][1] - corners[3][1]) ** 2)
    h = max(int(h1), int(h2))

    destination_corners = np.float32(
        [(0, h - 1), (0, 0), (w - 1, 0), (w - 1, h - 1)])


    return destination_corners, h, w


def unwarp(img, src, dst):
    """

    Args:
        img: np.array
        src: list
        dst: list

    Returns:
        un_warped: np.array

    """
    h, w = img.shape[:2]
    H, _ = cv2.findHomography(
        src, dst, method=cv2.RANSAC, ransacReprojThreshold=3.0)
    un_warped = cv2.warpPerspective(img, H, (w, h), flags=cv2.INTER_LINEAR)

    return un_warped


def apply_filter(image):
    """
    Define a 2X2 kernel and apply the filter to gray scale image
    Args:
        image: np.array

    Returns:
        filtered: np.array

    """
    gray = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
    kernel = np.ones((2, 2), np.float32) / 15
    filtered = cv2.filter2D(gray, -1, kernel)

    return filtered


def apply_threshold(filtered):
    """
    Apply OTSU threshold binary

    Args:
        filtered: np.array

    Returns:
        thresh: np.array

    """
    ret, thresh = cv2.threshold(filtered, 250, 255, cv2.THRESH_OTSU)

    return thresh



def warp(image):
    """
    Skew correction using homography and corner detection using contour points
    Returns: None

    """
    image_array = cv2.imread('img/input/demo/' + image)
    image_array  = cv2.cvtColor(image_array , cv2.COLOR_BGR2RGB)

    filtered_image = apply_filter(image_array )
    threshold_image = apply_threshold(filtered_image)

    cnv, largest_contour = detect_contour(threshold_image, image_array.shape)
    corners = detect_corners_from_contour(cnv, largest_contour)

    destination_points, h, w = get_destination_points(corners)
    un_warped = unwarp(image_array , np.float32(corners), destination_points)

    cropped = un_warped[0:h, 0:w]

    plt.set_cmap('hot')
    plt.axis('off')
    cropped = cv2.resize(cropped, dsize=(576,512) , interpolation= cv2.INTER_CUBIC ) 

    plt.imshow(cropped)

    name2 = image.split('.')[0]
    print(name2)

    

    plt.savefig('img/warped/test/' + name2  + '.png', bbox_inches='tight', dpi=136)
    im = Image.open("img/warped/test/" + name2  + ".png")
    rgb_im =im.convert('RGB')
    rgb_im.save('img/warped/test/' + name2  + '.jpeg')
    os.remove('img/warped/test/' + name2  + '.png')


if __name__ == '__main__':

    archivos = ls('./img/input/demo')

    pool = mp.Pool(mp.cpu_count())
    pool.map( warp ,[name for name in archivos] )
    pool.close()
    
    i= 1
    for name in archivos:
        name_cut = name.split('.')[0]
        os.rename('img/warped/test/'+name_cut+'.jpeg', 'img/warped/test/'+str(i)+'.jpeg')
        i = i+1


