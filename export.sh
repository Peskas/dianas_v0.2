#! /bin/sh

# Busca el ultimo checkpoint.data
X=0 && X=$(find -L ./training/ -name 'model.ckpt-*' | sort -V | tail -1)

# Corta los ultimos 20 caracteres para obtener el prefijo
L=${#X} && L=$(($L - 20)) && X=$(echo $X| cut -c 1-$L)

# Exporta el grafo de inferencia de la NN, que posteriormente sera utilizado para predecir
python3 export_inference_graph.py     --input_type image_tensor     --pipeline_config_path training/faster_rcnn_resnet101_coco.config     --trained_checkpoint_prefix $X     --output_directory training/diana_inference_graph