# ARCHIVO EN DESUSO

import numpy as np
import os
import matplotlib
from matplotlib import pyplot as plt
from cv2 import cv2
from PIL import Image
import random

# NUMPY REQUIREMENTS == 1.13.X < version < v.1.14.5
# Uncomment to see version
# print(np.__version__)
import tensorflow as tf

'''
# Archivos Drive 
import os
from google.colab import drive 
import zipfile
from shutil import copyfile

drive.mount('/content/drive', force_remount=True)

pathSrc = os.path.join(os.getcwd(),'drive','My Drive','diana','img.zip')

with zipfile.ZipFile(pathSrc,"r") as zip_ref:
    zip_ref.extractall()
'''

'''
Aplica 'warp perspective' a un conjunto de imagenes dado, con sus puntos a
aplicar la perspectiva y las dimensiones de las imegenes resultante. Tambien
se debe especificar si el tipo es 'train' o 'test', para guardar las imagenes
en la carpeta correspondiente
'''
def warp_imgs(W,H,ORIG_PTS,TYPE):
  absolute_path = os.path.join(os.getcwd(),'img','input')
  width,height = W,H
  i=1
  while i <= len(ORIG_PTS):
    if (TYPE == 'train'):
      img = cv2.imread(os.path.join(absolute_path,'dataset'+str(i)+'.jpeg'))
    elif (TYPE == 'test'):
      img = cv2.imread(os.path.join(absolute_path,'test'+str(i)+'.jpeg'))
    original_points = ORIG_PTS[i-1]
    dst_points = np.float32([[0,0],[width,0],[0,height],[width,height]])
    matrix = cv2.getPerspectiveTransform(original_points,dst_points)
    imgWarp = cv2.warpPerspective(img,matrix,(width,height))
    if (TYPE == 'train'):
      pathDest = 'img/warped/datasets/WARPEDdataset'+str(i)+'.jpeg'
    elif (TYPE == 'test'):
      pathDest = 'img/warped/datasets/WARPEDtest'+str(i)+'.jpeg'
    cv2.imwrite(pathDest,imgWarp)
    i = i + 1
  return i-1


'''
Funcion que, dada una imagen warpeada de la carpeta (img/warped), la divide en 8 y
las guarda en sus carpetas correspondientes en (img/warped/category)
'''
def split_imgs(NUMIMGS,TYPE):
  pathSrc = os.path.join(os.getcwd(),'img','warped','datasets')
  if (TYPE == 'train'):
    pathDst = os.path.join(os.getcwd(),'img','warped','train')
  elif (TYPE == 'test'):
    pathDst = os.path.join(os.getcwd(),'img','warped','test')
  x = 1
  numSubImg = 1
  while x <= NUMIMGS:
    if (TYPE == 'train'):
      img = Image.open(os.path.join(pathSrc,'WARPEDdataset'+str(x)+'.jpeg'))
    elif (TYPE == 'test'):
      img = Image.open(os.path.join(pathSrc,'WARPEDtest'+str(x)+'.jpeg'))
    nCol=2
    nFil=4
    dim = img.size
    w = dim[0]
    h = dim[1]
    newW = w/nCol
    newH = h/nFil
    for i in range(0,nFil):
      for j in range(0,nCol):
        X = j*newW
        Y = i*newH
        box = (X,Y,X+newW,Y+newH)
        subImg = img.crop(box)
        try:
          subImg.save(os.path.join(pathDst,str(numSubImg)+'.jpeg'))
          numSubImg=numSubImg+1
        except:
          pass
    x = x + 1


#########################
#       MAIN            #
#########################


def main():

  #################
  # WARP DATASETS #
  #################

  orig_w = 1152
  orig_h = 2048
  orig_pts = []

  # Puntos para realizar el warp (Introducidos manualmente)
  orig_pts.append(np.float32([[109,296],[1050,305],[27,1714],[1120,1710]]))
  orig_pts.append(np.float32([[136,419],[1005,450],[29,1768],[1110,1768]]))
  orig_pts.append(np.float32([[80,263],[1038,244],[36,1670],[1108,1664]]))
  orig_pts.append(np.float32([[132,330],[1074,345],[56,1717],[1100,1744]]))
  orig_pts.append(np.float32([[66,203],[1151,190],[38,1706],[1115,1737]]))
  orig_pts.append(np.float32([[52,289],[1120,279],[2,1850],[1151,1856]]))
  orig_pts.append(np.float32([[31,190],[1098,180],[43,1735],[1143,1688]]))
  orig_pts.append(np.float32([[52,327],[1119,326],[28,1881],[1138,1850]]))
  orig_pts.append(np.float32([[26,343],[1095,312],[36,1903],[1151,1877]]))
  orig_pts.append(np.float32([[10,307],[1090,304],[12,1887],[1098,1850]]))
  orig_pts.append(np.float32([[34,223],[1114,258],[0,1818],[1125,1794]]))
  orig_pts.append(np.float32([[34,270],[1151,281],[16,1879],[1151,1876]]))
  orig_pts.append(np.float32([[51,267],[1112,247],[70,1786],[1143,1756]]))
  orig_pts.append(np.float32([[53,274],[1090,273],[38,1780],[1121,1758]]))
  orig_pts.append(np.float32([[47,261],[1092,233],[39,1765],[1138,1752]]))
  i = warp_imgs(orig_w,orig_h,orig_pts,'train')

  # Dividir las hojas entre 8 para obtener las dianas individuales
  split_imgs(i,'train')

  orig_pts = []
  orig_pts.append(np.float32([[64,330],[1088,310],[41,1830],[1151,1818]]))
  orig_pts.append(np.float32([[0,552],[698,446],[163,1596],[1115,1349]]))
  i = warp_imgs(orig_w,orig_h,orig_pts,'test')
  split_imgs(i,'test')

main()