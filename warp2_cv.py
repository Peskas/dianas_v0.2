# ARCHIVO NO FUNCIONAL
import cv2
import os
import numpy as np
import matplotlib.pyplot as plt

# PRUEBA DEL WARP CON HOMOGRAPHY (Necesario opencv-python==3.4.2.16 y opencv-contrib-python==3.4.2.16)
img1 = cv2.imread(os.getcwd()+'/img/input/demo/1.jpg',1)
img2 = cv2.imread(os.getcwd()+'/img/input/demov2/diana.jpg',1)

import numpy as np
from matplotlib import pyplot as plt
MIN_MATCH_COUNT = 5
# Initiate SIFT detector
sift = cv2.xfeatures2d.SIFT_create()
# find the keypoints and descriptors with SIFT
kp1, des1 = sift.detectAndCompute(img1,None)
kp2, des2 = sift.detectAndCompute(img2,None)
FLANN_INDEX_KDTREE = 1
index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
search_params = dict(checks = 50)
flann = cv2.FlannBasedMatcher(index_params, search_params)
matches = flann.knnMatch(des1,des2,k=2)
# store all the good matches as per Lowe's ratio test.
good = []
for m,n in matches:
    if m.distance < 0.7*n.distance:
        good.append(m)

if len(good)>MIN_MATCH_COUNT:
    src_pts = np.float32([ kp1[m.queryIdx].pt for m in good ]).reshape(-1,1,2)
    dst_pts = np.float32([ kp2[m.trainIdx].pt for m in good ]).reshape(-1,1,2)
    M, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC,5.0)
    matchesMask = mask.ravel().tolist()
    h,w,d = img1.shape
    pts = np.float32([ [0,0],[0,h-1],[w-1,h-1],[w-1,0] ]).reshape(-1,1,2)
    print(str(pts))
    dst = cv2.perspectiveTransform(pts,M)
    img2 = cv2.polylines(img2,[np.int32(dst)],True,255,3, cv2.LINE_AA)
else:
    print( "Not enough matches are found - {}/{}".format(len(good), MIN_MATCH_COUNT) )
    matchesMask = None

draw_params = dict(matchColor = (0,255,0), # draw matches in green color
                   singlePointColor = None,
                   matchesMask = matchesMask, # draw only inliers
                   flags = 2)
img3 = cv2.drawMatches(img1,kp1,img2,kp2,good,None,**draw_params)
plt.imshow(img3, 'gray'),plt.show()


# HOMOGRAPHY
#orb = cv2.ORB_create()
#
#kp1, des1 = orb.detectAndCompute(img1,None)
#kp2, des2 = orb.detectAndCompute(img2,None)
#
#bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck = True)
#
#matches = bf.match(des1, des2)
#matches = sorted(matches, key = lambda x:x.distance)
#
#print(str(len(matches)))
#img3 = cv2.drawMatches(img1,kp1,img2,kp2, matches[0:], None, flags=2)
#plt.imshow(img3)
#plt.show()


# CORNER DETECTION
#import numpy as np
#import cv2
#
#gray = cv2.cvtColor(img1,cv2.COLOR_BGR2GRAY)
#gray = np.float32(gray)
#
#corners = cv2.goodFeaturesToTrack(gray, 100, 0.01, 10)
#corners = np.int0(corners)
#
#for corner in corners:
#    x,y = corner.ravel()
#    cv2.circle(img1,(x,y),3,255,-1)
#    
#cv2.imshow('Corner',img1)
#cv2.waitKey(0)


