.PHONY: build

# Lo primero de todo es descargar el modelo del drive, descomprimirlo en la carpeta
# 	 raiz y ejecutar los siguientes comandos desde la carpeta raiz:
#	 cd training/models/research
#	 export PYTHONPATH=$PYTHONPATH:`pwd`:`pwd`/slim
#	 cd ./../../..

# Instala las librerias de python necesarias para utilizar la red neuronal.
# Asegurate una vez finalizada la instalacion que las librerias que requieran
# una version especifica, se encuentren en dicha version, para ello:
# pip3 show <libreria> | pip3 uninstall <libreria>==X.X.X | pip3 install <libreria>==X.X.X
install: 
	pip3 install scipy; \
	pip3 install matplotlib; \
	pip3 install pillow; \
	pip3 install pandas; \
	pip3 install gast==0.2.2; \
	pip3 install Cython; \
	pip3 install contextlib2; \
	pip3 install lxml; \
	pip3 install setuptools; \
	pip3 install PyQt5; \
	pip3 install tensorflow==1.14.0; \
	pip3 install opencv-python; \
	pip3 uninstall numpy; \
	pip3 install numpy==1.14.5; \


# Aplica el efecto de warp a las imagenes de "img/input/demo" y las resultantes se guardan
# en "img/warped/test" para utilizar en la demo. Tambien hemos utilizado este script para
# generar las fotos warpeadas de entrenamiento.
warp:
	python3 warp_cv.py;


# Limpia la red neuronal incluidas las etiquetas (.csv), sus checkpoints y grafo de
# inferencia.
clean-neural:
	rm -f ./training/*events*; \
	rm -f ./training/model.ckpt* \
	rm -f -rf ./training/diana_inference_graph/; \
	rm -f ./training/checkpoint; \
	rm -f ./training/*.record*; \
	rm -f ./data/*labels*; \


# Construye los archivos record a partir de las etiquetas para entrenar la red.
build-neural:
	python3 xml_to_csv.py; \
	python3 generate_tfrecord.py --csv_input=data/train_labels.csv --output_path=training/train.record --image_dir=img/warped/train; \
	python3 generate_tfrecord.py --csv_input=data/test_labels.csv --output_path=training/test.record --image_dir=img/warped/test;


# Entrena la red en base a los archivos record generados. Para el correcto funcionamiento,
# debes ejecutar desde la carpeta raiz:
#	 cd training/models/research
#	 export PYTHONPATH=$PYTHONPATH:`pwd`:`pwd`/slim
#	 cd ./../../..
train-neural:
	(cd training/models/research/slim/ && sudo python3 setup.py build); \
	(cd training/models/research/slim/ && sudo python3 setup.py install); \
	python3 training/models/research/object_detection/legacy/train.py --logtostderr --train_dir=training/ --pipeline_config_path=training/faster_rcnn_resnet101_coco.config;

# Guarda la red, es decir, exporta el grafo de inferencia segun el ultimo checkpoint generado
# entrenando. Este grafo es necesario posteriormente para predecir las imagenes de test.
save-neural:
	rm -f -rf training/diana_inference_graph/; \
	L=realpath ./export.sh; \
	. /$(L);


# Predice la puntuacion de las dianas de las imagenes situadas en la carpeta "img/warped/test/".
# Para su correcto funcionamiento, las imagenes deben estar en formato ".jpeg" y estar nombradas
# unicamente con numeros, p.e. "1.jpeg". Ademas, es recomendable que esten por orden numerico
# para un testeo mas comodo. Por defecto testeara las imagenes (1-4), pero se pueden introducir
# los parametros "N_INI" y "N_FIN" para especificar las imagenes a predecir. Tambien se puede
# especificar el modo de prediccion (mostrar/guardado).
#
#	Ejemplo:
# 	 make test N_INI=1 N_FIN=10 MODE=show
#
test:
	cd testing/; \
	python3 object_detection.py $(N_INI) $(N_FIN) --mode=$(MODE)


# Aplica Warp Perspective paso a paso para la imagen indicada en el parametro "NAME" permitiendo 
# observar los distintos filtros y tecnicas aplicadas.
# Esta imagen ha de estar en el directorio  /skew_correction/images/
#
#	Ejemplo:
# 	 make warp_steps NAME=1.jpeg
#
warp_steps:
	cd skew_correction/; \
	python3 skew_correction.py $(NAME) 