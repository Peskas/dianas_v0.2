# Práctica Voluntaria. Diana (Neural Network)

Autores: Ángel Bernáldez Pereda y Javier Irulegui Parra  
Fecha: Diciembre 2019 - Enero 2020  

## Historial

Al comienzo de esta práctica apenas habíamos trabajado con redes neuronales (hicimos una hackathon y las comprendimos en su momento, pero no llegamos a hacer funcionar ninguna), por lo que ha sido un gran reto conseguir resultados aceptables.  

<u>PRIMERA SEMANA (Iru).</u>  
La primera semana la hemos dedicado a construir el dataset a utilizar, este es, el conjunto de imágenes con dianas dado en el pdf de muestra, con agujeros generalmente homogéneos generados con un lapicero. Esta función la hemos conseguido utilizando *"Python3"* en el archivo *"prepare_imgs*" en el que utilizamos las librerías *"numpy"*, *"matplotlib"*, *"cv2"* y *"pillow"*.  

Por una parte, en los datos de entrada tenemos un conjunto de 15 imágenes con 8 dianas cada una. A estas tenemos que aplicar *"warp-perspective"* para normalizar los datos de entrada, de forma que se consiga una imagen lo más regular posible. No hemos conseguido aplicar una red neuronal sobre las imágenes de entrada para "auto-detectar" las esquinas del folio a warpear, por lo que las esquinas son introducidas manualmente. Esto lo realiza la función *"warp_imgs()"*, la cuál recibe las nuevas dimensiones de las imágenes warpeadas, y el conjunto de puntos a warpear para las imágenes originales. Las imágenes "datasetN.jpeg" se cargan de la carpeta "/img/input/" y las imágenes warpeadas se guardan en la carpeta "/img/warped/datasets" como "WARPED+NOMBRE_ORIG.jpeg". Finalmente la función retorna el número de imágenes warpeadas, que sirve de utilidad para la siguiente función.  

Una vez warpeados los datasets, necesitamos recortar las imágenes en 8 partes para obtener las dianas individuales. Esto lo conseguimos con la función *"split_imgs()"*. Esta función recibe por parámetro el número de imágenes a cortar, el cual es retornado por la función de warp. Para cortar las imágenes, coge de la carpeta "/img/warped/datasets" el conjunto de datasets warpeados, y los recorta en 8 partes iguales con 2 bucles *for* y mediante la función *"crop()"* del paquete *"Pillow"*. Estas imágenes cortadas con las dianas individules se guardan en la carpeta "/img/warped/imgs".  

Este paso ha quedado en desuso tras aplicar la nueva warp perspective, puesto que ahora necesitamos fotos de dianas individuales, no de un dataset de 8 dianas.  


<u>SEGUNDA SEMANA (Iru y Peskas).</u>  

(Iru y Peskas) Después de tener el dataset con las imágenes individuales, es hora de crear la red neuronal. Al principio, hemos intentado crear de 0 nuestra propia NN, intentando clasificar las imágenes enteras según los agujeros (esto es, en categorías con las posibles combinaciones de agujeros), sin acotar ninguna zona. Pero la red neuronal no aprendía puesto que no encontraba un patrón por sí sola, llegando a dar resultados nefastos.  

(Iru) Después, hemos investigado más, y hemos optado por utilizar un modelo de detección de objetos preestablecido. Este es una Red Neuronal Convulacional específica para la detección de objetos. Primero habíamos entrenado la red de forma que detectase 4 tipos de objetos, estos son los 4 tipos de agujeros. Esto lo conseguíamos etiquetando los agujeros de los datasets de entrenamiento de forma que claramente se distinga un patrón compuesto del agujero con el contexto en el que se encuentra; esto es, fondo blanco/gris y patrones de los anillos. Este resultado fue positivo, puesto que no tuvimos que aplicar ninguna algoritmia para la detección de agujeros, pero tenía un ligero inconveniente. Ya que el etiquetado debía no ser ajustado al agujero de forma que detectase el patrón que forma con la diana, al producirsse solapes, o agujeros próximos entre sí, fallaba de forma que detectaba varios agujeros en uno, o uno en vez de varios. Tras quedar contigo optamos por cambiar la forma de entrenar la red; simplemente detectar agujeros y esquinas y calcular la puntuación de los agujeros en función de la posición de los agujeros detectados respecto al centro de las 4 esquinas.



<u>TERCERA SEMANA (Peskas).</u>  
<u>Auto-Warp.</u>  

Hemos desarrollado una función previa a la red neuronal utilizada para warpear una diana de una foto a un recorte de esta centrado y escalado. Para ello se toman distintas funciones de openCV, empezamos convirtiendo la foto en escala de grises, despues usaremos la función threshold binary, la cual convierte la escala de grises en blanco y negro puro, asociando estos en funcion de la tonalidad del gris, esos tonos permiten delimitar muy claramente el área del folio que contiene la diana. A continuación detectaremos el contorno de este área utilizando la función de OpenCV para este propósito, una vez detectemos este contorno, calcularemos donde se encuentran los vértices de este mediante la aproximación dada por ocv.approxPolyDP, estos vértices se ordenarán de forma que uniendo los vértices consecutivos obtengamos un polígono mediante una función desarrollada expresamente para este fin.  
Una vez obtenidas las coordenadas de los vértices calcularemos los puntos de destino para los nuevos vértices y la matriz de transición entre la imagen original y el recorte de la diana, estos datos serán introducidos en la función warp perspective de OpenCV la cual completará el trabajo.  
Adicionalmente, cambiaremos el formato de la imagen para guardarla finalmente en un jpeg, además renombraremos todas las imagenes producidas. 
La función warp_cv.py leerá todas las imágenes del directorio img/input/demo y las procesará de forma paralela, haciendo uso de un threadpool (un thread por foto) y guardándolas en el directorio img/warped/test.  

<u>CUARTA SEMANA (Iru y Peskas).</u>  

En esta semana hemos reentrenado la red aplicando el auto-warp anterior y etiquetando de nuevo las imágenes con los objetos necesarios (esquinas, agujeros), pero esta vez con menos imágenes de entrada, puesto solo necesitamos entrenar la red para detectar agujeros y esquinas, que son objetos que no tienen mucha variabilidad en las imágenes, a diferencia del anterior modelo que necesitábamos un dataset amplio para abarcar todas las posibilidades de patrones de los agujeros. También hemos orientado el dataset para reforzar la red con distintos colores de fondos para distintas imágenes, de forma que simulen un caso más real.  

Este proceso ha sido llevado por ambos, (Peskas) ha organizado de nuevo la estructura de archivos del repositorio, quedando así una visión más clara de las imágenes de entrada para una futura demo. Además, con la función que (Iru) ha desarrollado para devolver las coordenadas de los objetos predecidos, (Peskas) ha calculado la puntuación de estos en función de su posición en la diana respecto al centro. Esta puntuación se consigue comprobando si la posición del agujero se encuentra dentro del área que forma el óvalo que forma cada anillo, cuyos centro y radio se calculan con las esquinas detectadas, y con una proporción de cada anillo respecto el ancho y alto de la imagen, respectivamente. Esta relación ha sido calculada con matemática básica respecto a la diana original. Esto provoca que los fallos sean mínimos, puesto que el área del óvalo se adapta a la posible deformación producida al aplicar warp-perspective.  

(Iru), por otra parte, se ha dedicado a corregir pequeños detalles de la conexión de los archivos, generar archivos makefile para un cómodo uso y una visualización más clara de las predicciones de test, incluyendo la puntuación de cada agujero en la propia label de su bounding box, de forma que el resultado final quede más limpio. Además, ha ayudado a (Peskas) a pasarle los datos de los objetos detectados, como hemos mencionado en el párrafo anterior.


## Demo Prediccion

Modelo entrenado: https://drive.google.com/file/d/1GTAnYySRYy1SD6C16hf9FKCQSdR6WROU/view?usp=sharing  

Para efectuar la demo primero deberemos descargar nuestro modelo pre-entrenado del drive, y posteriormente descomprimirlo en la carpeta raíz del directorio. Una vez descomprimido, debemos ejecutar las líneas de código descritas al principio del makefile, aparte de ejecutar el **make install** para instalar las dependencias necesarias. Acto seguido, introduciremos las fotografías de las dianas a predecir en el directorio "/img/input/demo/". Estas fotografías han de estar nombradas sin más puntos "." que el de la propia extensión del archivo y preferiblemente en formato ".jpeg". Es recomendable que estas fotografías se hagan sobre superficies de un color distinto al blanco de la diana y que no presenten demasiado brillo, además procuraremos realizar la fotografía lo suficientemente cerca de la diana.  

| | |
| - | - | - |
| ![](img/report/modelo.png) | ![](img/report/comandos.png) |

<div style="text-align:center"><img src="img/report/carpeta.png" alt="drawing" style="width:400px;"/>
</div>

Ejecutaremos primero **make warp** lo cual cogerá nuestras fotos, aplicará warp prespective sobre ellas y las guardará en el fichero /img/warped/test/ , podremos comprobar ahí el resultado del warp en nuestras imágenes para detectar si alguna no se ha realizado correctamente, si alguna foto no se ha realizado correctamente y ha perdido alguna de las esquinas esta será ignorada durante el proceso de test.  

| | |
| - | - | - |
| ![](img/report/warp.png) | ![](img/report/warped.png) |  
&nbsp;  

Si queremos comprobar el proceso del warp perspective paso a paso, comprobando la ejecución de los distintos filtros podemos utilizar el comando **make warp-steps**, para ello previamente introduciremos la imagen a comprobar en el directorio /skrew_correction/images/ y anadiremos el nombre de la imagen como parámetro a la función make (ver makefile).  

Despues predeciremos los agujeros y sus puntuaciones en las imágenes procesadas, para ello ejecutaremos el comando **make test** indicando el número de imagen inicial y final a predecir mediante los parametros N_INI y N_FIN , ademas indicaremos si queremos guardar o mostrar la imagen con las predicciones mediante el parámetro MODE (ver makefile). 
Si elegimos el modo de visualización las imágenes se irán mostrando a medida que se generen, en cambio si elegimos el modo para guardar estas se almacenaran en el directorio "/testing/predict/".  

| | |
| - | - | - |
| ![](img/report/test.png) | ![](img/report/predict.png) |  
&nbsp;  


<hr/>

## Requirements

><u><font size=3.5>LANGUAJE</font></u>  
>python3.6
>  
><u><font size=3.5>LIBRARIES</font> (pip3 install)</u>  
>tensorflow==1.14.0  
>numpy==1.14.5  
>scipy
>matplotlib  
>cv2 (opencv-python)  
>pillow  
>pandas  
>gast==0.2.2  
>Cython  
>contextlib2  
>lxml  
>setuptools  
>PyQt5  

<hr/>

## Usage

**DOWNLOAD TRAINED MODEL AND UNZIP IT TO ROOT FOLDER**  

https://drive.google.com/file/d/1GTAnYySRYy1SD6C16hf9FKCQSdR6WROU/view?usp=sharing

<hr/>

## Extended Usage

**<u>1. LOAD YOUR OWN DATA</u>** 

**Run warp_cv.py**

```sh
# PREPARE THE IMG DATASET FROM "img/input/demo" FOLDER.
# ALL IMGS ARE SAVED IN "img/warped/test" FOLDER.
# THIS FILE WARP THE ORIGINAL DATASET TO NORMALIZE IMGS.

    python3 warp_cv.py
```

**Create dataset labels from warped dartboards**

```sh
# DOWNLOAD LABEL-IMG AND RUN WITH

    python3 labelImg.py  
  
# OPEN "/img/warped/train" FOLDER AND LABEL ALL IMAGES WITH 4 OBJECT TYPES  
# {agujero0,agujero1,agujero2,agujero3}  
# SAVE ALL .XML FILES
```

**Create TFRecord files**

```sh
# GENERATE CSV LABEL MAP ("train_labels" and "test_labels" into "data/" folder)

    python3 xml_to_csv.py


# GENERATE .RECORD FILES

    cd training/models/research && export PYTHONPATH=$PYTHONPATH:`pwd`:`pwd`/slim
    cd ./../../..

    # FOR TRAIN
    python3 generate_tfrecord.py --csv_input=data/train_labels.csv --output_path=training/train.record --image_dir=img/warped/train  

    # FOR TEST
    python3 generate_tfrecord.py --csv_input=data/test_labels.csv --output_path=training/test.record --image_dir=img/warped/test  
```

<hr/>

**<u>2. TRAIN THE MODEL</u>** 

```sh
    # RUN SETUP.PY
    (cd training/models/research/ && sudo python3 setup.py build)
    (cd training/models/research/ && sudo python3 setup.py install)


    # TRAIN YOUR MODEL
    python3 training/models/research/object_detection/legacy/train.py --logtostderr --train_dir=training/ --pipeline_config_path=training/faster_rcnn_resnet101_coco.config

    # GENERATE INFERENCE GRAPH
    python3 export_inference_graph.py     --input_type image_tensor     --pipeline_config_path training/faster_rcnn_resnet101_coco.config     --trained_checkpoint_prefix training/model.ckpt-(NUM. OF CKPT)     --output_directory training/diana_inference_graph

```

<hr/>

**<u>3. TEST THE MODEL</u>** 

```sh
    # PREDICT SOME RESULTS FROM "img/warped/test/" FOLDER.
    # YOU MUST SPECIFY FROM THE NUMBER/NAME IMG TO THE 
    # NUMBER/NAME IMG YOU WANNA TEST. ALSO YOU CAN SPECIFY
    # THE DISPLAY MODE (SAVE IMGS OR SHOW THEM)
    
    # (from "testing/" folder run:)
    python3 object_detection.py <N_INI> <N_FIN> [--mode=save/show]
```